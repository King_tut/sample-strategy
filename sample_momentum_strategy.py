#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import numpy as np
import pandas as pd
import arrow
import talib
import quandl
import random

import requests
import json
import time
import keyring
import datetime
from datetime import date
from datetime import datetime

import pandas_market_calendars as mcal
import math
from math import floor

import sys
import datetime as dt
import csv
import string
import tenacity
from tenacity import retry
from dateutil import parser

from iexfinance import Stock
from iexfinance import get_historical_data

from pykalman import KalmanFilter

from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation, Flatten
from keras.layers.recurrent import LSTM, GRU
from keras.layers import Convolution1D, MaxPooling1D, AtrousConvolution1D, RepeatVector
from keras.callbacks import ModelCheckpoint, ReduceLROnPlateau, CSVLogger
from keras.layers.wrappers import Bidirectional
from keras import regularizers
from keras.layers.normalization import BatchNormalization
from keras.layers.advanced_activations import *
from keras.optimizers import RMSprop, Adam, SGD, Nadam
from keras.initializers import *
from keras.constraints import max_norm

import alpaca_trade_api as tradeapi

from pandas_datareader import data as pdr

import fix_yahoo_finance as yf
yf.pdr_override()


print ("running")


def get_data(ticker, startdate):
    #example tickers= stocks:"SPY"; forex:"GBPUSD=X";crypto:"BTC-USD"
    df=pdr.get_data_yahoo( ticker, start=startdate)
    return df




def start():
    
    
    api = tradeapi.REST(
    key_id='XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
    secret_key='XXXXXXXXXXXXXXXXXXXXXXXXXXX',
    base_url='https://paper-api.alpaca.markets')

    return  api


def filterdata(df):
    # Filter noise
    kf = KalmanFilter(transition_matrices = [1],
                observation_matrices = [1],
                initial_state_mean = 0,
                initial_state_covariance = 1,
                observation_covariance=1,
                transition_covariance=.01, em_vars=['all'])
    kf = kf.em(np.asarray(df), n_iter=1,  em_vars='all')
    state_means, _= kf.filter(df.values)
    state_means = pd.Series(state_means.flatten(), index=df.index)
    
    rsi= talib.STDDEV(state_means.values, timeperiod=14)/100
    
    return rsi[15:]



def extractnumber(s):
    k= s.replace("'," ,"")
    j=k.replace("'" ,"")
    m=j.replace("," ,"")
    n=round(float(m),6)
    return n

def get_cash():
    account = api.get_account()
    s=str(account).split()
    return (extractnumber(s[6]))

def get_portfolio_value():
    #total
    account = api.get_account()
    s=str(account).split()
    return (extractnumber(s[18]))

def get_position(symbol):
    try:
        return float(api.get_position(symbol))
    except:
        return 0

def marketorder(symbol, quantity, side):
    api.submit_order(symbol,quantity,side,
            type='market',
            time_in_force='day',
        )

def get_price(symbol):
    stk=Stock(symbol, output_format='pandas')
    return float(stk.get_price().loc[symbol])
        
def get_bid_ask(symbol):
    stk=Stock(symbol, output_format='pandas')
    ask= float(stk.get_quote().loc['iexAskPrice'])
    bid= float(stk.get_quote().loc['iexBidPrice'])

    if bid== 0 or ask ==0:
        ask= float(stk.get_price().loc[symbol])
        bid= float(stk.get_price().loc[symbol])
    else:
        pass
    return bid, ask   
 

def fraction_and_percent(symbol):
    
    totalbalance= get_portfolio_value()
    portfolio=get_position(symbol)
    price=get_price(symbol)
    fraction=portfolio*price/totalbalance
    percent=round(fraction*100,0)
    return fraction, percent

def twap(x):
    a=int(math.floor(x))

    list=[a]
    return list


def ordersizes(targetweight,fraction, symbol):
    currentprice=get_price(symbol)
    totalbalance=get_portfolio_value()

    buyfraction=targetweight-fraction
    stocksstobuy=totalbalance*buyfraction/currentprice

    sellfraction=fraction-targetweight
    stocksstosell=totalbalance*sellfraction/currentprice

    if (targetweight>fraction):
        return twap(stocksstobuy), "buy"
    elif (targetweight<fraction):
        return twap(stocksstosell), "sell"
    else:
        return [0], "pass"

def orderpercent(symbol, targetweight,stage):
    
    fraction,percent=fraction_and_percent(symbol)
    
    ordersize,ordername=ordersizes(targetweight,fraction, symbol)
    
    
    print ('ordering')
    print (symbol)
    r1=random.uniform(0.2,0.4)
    r2=random.uniform(0.15,0.3)
    r3=random.uniform(0.15,0.3)
    r4=1-r1-r2-r3

    amount = int(math.floor(ordersize[stage]))

    amount_r1 = int(math.floor(ordersize[stage]*r1))

    amount_r2 = int(math.floor(ordersize[stage]*r2))

    amount_r3 = int(math.floor(ordersize[stage]*r3))

    amount_r4 = int(math.floor(ordersize[stage]*r4))

    if ordername=='buy':

        print ("market order buy")
        
        if  get_cash()>float(amount)*get_price(symbol):
            marketorder(symbol, amount, 'buy')
            
        else:
            if  get_cash()>float(amount_r1)*get_price(symbol):

                marketorder(symbol, amount_r1, 'buy')
            else:
                pass

            if  get_cash()>float(amount_r2)*get_price(symbol):

                marketorder(symbol, amount_r2, 'buy')

                a=random.uniform(1,5)
                time.sleep(a)
            else:
                pass

            if  get_cash()>float(amount_r3)*get_price(symbol):

                marketorder(symbol, amount_r3, 'buy')

                a=random.uniform(1,5)
                time.sleep(a)
            else:
                pass

            if  get_cash()>float(amount_r4)*get_price(symbol):

                marketorder(symbol, amount_r4, 'buy')
                a=random.uniform(1,5)
                time.sleep(a)
            else:
                pass

        bid, ask=get_bid_ask(symbol)
        trade = [arrow.now('US/Eastern').date(),"buy",symbol,bid,round(ordersize[stage],4), "market"]

        with open(r'trades.csv', 'a') as f:
            writer = csv.writer(f, delimiter=',')
            writer.writerow(trade)


    elif ordername=='sell':

        print ("market order sell")
        

        marketorder(symbol, amount, 'sell')

        bid, ask=get_bid_ask(symbol)
        trade = [arrow.now('US/Eastern').date(),"sell",symbol,bid,round(ordersize[stage],4),"market"]
        with open(r'trades.csv', 'a') as f:
           writer = csv.writer(f, delimiter=',')
           writer.writerow(trade)

    else:
        print ('pass')
        pass


def shuffle_in_unison(a, b):
    # courtsey http://stackoverflow.com/users/190280/josh-bleecher-snyder
    assert len(a) == len(b)
    shuffled_a = np.empty(a.shape, dtype=a.dtype)
    shuffled_b = np.empty(b.shape, dtype=b.dtype)
    permutation = np.random.permutation(len(a))
    for old_index, new_index in enumerate(permutation):
        shuffled_a[new_index] = a[old_index]
        shuffled_b[new_index] = b[old_index]
    return shuffled_a, shuffled_b


def create_Xt_Yt(X, y, percentage=0.7):
    p = int(len(X) * percentage)
    X_train = X[0:p]
    Y_train = y[0:p]

    X_train, Y_train = shuffle_in_unison(X_train, Y_train)

    X_test = X[p:]
    Y_test = y[p:]

    return X_train, X_test, Y_train, Y_test


def remove_nan_examples(data):
    newX = []
    for i in range(len(data)):
        if np.isnan(data[i]).any() == False:
            newX.append(data[i])
    return newX
    

def make_model(df,file ):
    #deleted
    pass
    
def get_wieght(df,one,two,three,four,five):
     #deleted
    pass
    


    
burger=1
while (burger<10):
    
    onespy="Onespy_public.hdf5"
    twospy="Twospy_public.hdf5"
    threespy="Threespy_public.hdf5"
    Fourspy="Fourspy_public.hdf5"
    Fivespy="Fivespy_public.hdf5"
    
    onetlt="Onetlt_public.hdf5"
    twotlt="Twotlt_public.hdf5"
    threetlt="Threetlt_public.hdf5"
    Fourtlt="Fourtlt_public.hdf5"
    Fivetlt="Fivetlt_public.hdf5"
    
    onegld="Onegld_public.hdf5"
    twogld="Twogld_public.hdf5"
    threegld="Threegld_public.hdf5"
    Fourgld="Fourgld_public.hdf5"
    Fivegld="Fivegld_public.hdf5"
    
    oneefa="Oneefa_public.hdf5"
    twoefa="Twoefa_public.hdf5"
    threeefa="Threeefa_public.hdf5"
    Fourefa="Fourefa_public.hdf5"
    Fiveefa="Fiveefa_public.hdf5"
    
    onevwo="Onevwo_public.hdf5"
    twovwo="Twovwo_public.hdf5"
    threevwo="Threevwo_public.hdf5"
    Fourvwo="Fourvwo_public.hdf5"
    Fivevwo="Fivevwo_public.hdf5"
    
    onexop="Onexop_public.hdf5"
    twoxop="Twoxop_public.hdf5"
    threexop="Threexop_public.hdf5"
    Fourxop="Fourxop_public.hdf5"
    Fivexop="Fivexop_public.hdf5"
    
    oneqqq="Oneqqq_public.hdf5"
    twoqqq="Twoqqq_public.hdf5"
    threeqqq="Threeqqq_public.hdf5"
    Fourqqq="Fourqqq_public.hdf5"
    Fiveqqq="Fiveqqq_public.hdf5"
    

    onexlk="Onexlk_public.hdf5"
    twoxlk="Twoxlk_public.hdf5"
    threexlk="Threexlk_public.hdf5"
    Fourxlk="Fourxlk_public.hdf5"
    Fivexlk="Fivexlk_public.hdf5"
    
    onepall="Onepall_public.hdf5"
    twopall="Twopall_public.hdf5"
    threepall="Threepall_public.hdf5"
    Fourpall="Fourpall_public.hdf5"
    Fivepall="Fivepall_public.hdf5"
    
    
    
    
    
    
    g=arrow.now('US/Eastern')
    hour= g.time().strftime("%H:%M:%S" )
    

    nyse = mcal.get_calendar('NYSE')
    early = nyse.schedule(start_date='2018-07-01', end_date='2030-07-10')

    marketstatus= nyse.open_at_time(early, pd.Timestamp(str(str(date.today())+g.time().strftime(" %H:%M")),
                                          tz='America/New_York'))
    
    
    if hour=="09:30:30" and marketstatus==True  :  
        
        api = start()
        
        spy_df= get_data('SPY', "1993-01-29")
        
        tlt_df= get_data('TLT', "2002-07-30")
        
        gld_df= get_data('GLD', "2004-11-18")
        
        xlk_df= get_data('XLK', "1998-12-22")
        
        vwo_df= get_data('VWO', "2005-03-10")
        
        efa_df= get_data('EFA', "2001-08-27")
        
        xop_df= get_data('XOP', "2006-06-22")
        
        qqq_df= get_data('QQQ', "1999-03-10")
        
        pall_df= get_data('PALL', "2010-10-14")
        
    
        spywieght, spyguess= get_wieght(spy_df, onespy,twospy, threespy,Fourspy,Fivespy)

        tltwieght, tltguess= get_wieght(tlt_df, onetlt,twotlt, threetlt,Fourtlt,Fivetlt)

        gldwieght, gldguess= get_wieght(gld_df, onegld,twogld, threegld,Fourgld,Fivegld)

        xlkwieght, xlkguess= get_wieght(xlk_df, onexlk,twoxlk, threexlk,Fourxlk,Fivexlk)

        vwowieght, vwoguess= get_wieght(vwo_df, onevwo,twovwo, threevwo,Fourvwo,Fivevwo)

        efawieght, efaguess= get_wieght(efa_df, oneefa, twoefa, threeefa,Fourefa, Fiveefa)
        qqqwieght, qqqguess= get_wieght(qqq_df, oneqqq,twoqqq, threeqqq,Fourqqq,Fiveqqq

        xopwieght, xopguess= get_wieght(xop_df, onexop,twoxop, threexop,Fourxop,Fivexop)

        pallwieght, pallguess= get_wieght(pall_df, onepall,twopall, threepall,Fourpall,Fivepall)
        
        
        
        
        
       

        total=(spywieght+tltwieght+gldwieght+xopwieght+xlkwieght+vwowieght+efawieght+qqqwieght+pallwieght+0.00000001)

        targetspy=abs((spyguess*spywieght/total))*0.95

        targetgld=abs((gldguess*gldwieght/total))*0.95

        targettlt=abs((tltguess*tltwieght/total))*0.95

        targetvwo=abs((vwoguess*vwowieght/total) )*0.95

        targetxlk=abs((xlkguess*xlkwieght/total) )*0.95

        targetefa=abs((efaguess*efawieght/total))*0.95

        targetqqq=abs((qqqguess*qqqwieght/total))*0.95

        targetxop= abs((xopguess*xopwieght/total))*0.95

        targetpall= abs((pallguess*pallwieght/total))*0.95

        

        UPROsymbol='UPRO'#spy 3x leveraged

        TMFsymbol='TMF'#tlt 3x leveraged
        UGLDsymbol='UGLD'#gld 3x leveraged
        TQQQsymbol='TQQQ'#qqq 3x leveraged

        GUSHsymbol='GUSH'#xop 3x leveraged
        DZKsymbol='DZK'#EFA" developed equities 3x leveraged

        EDCsymbol='EDC'#VWO emerging markets 3x leveraged
        TECLsymbol='TECL'#xlk 3x leveraged

        PALLsymbol='PALL'#palladium 
        
        
        UPRO_fraction, UPRO_p=fraction_and_percent(UPROsymbol)

        TMF_fraction, TMF_p=fraction_and_percent(TMFsymbol)

        UGLD_fraction, UGLD_p=fraction_and_percent(UGLDsymbol)


        TQQQ_fraction, TQQQ_p=fraction_and_percent(TQQQsymbol)

        GUSH_fraction, GUSH_p=fraction_and_percent(GUSHsymbol)

        EDC_fraction, EDC_p=fraction_and_percent(EDCsymbol)

        TECL_fraction, TECL_p=fraction_and_percent(TECLsymbol)

        DZK_fraction, DZK_p=fraction_and_percent(DZKsymbol)

        PALL_fraction, PALL_p=fraction_and_percent(PALLsymbol)

        
        
        change_UPRO=(targetspy)- UPRO_fraction
        change_UGLD=(targetgld)- UGLD_fraction
        change_TMF=(targettlt)- TMF_fraction
        change_EDC=(targetvwo)- EDC_fraction

        change_TECL=(targetxlk)- TECL_fraction
        change_DZK=(targetefa)- DZK_fraction
        change_TQQQ=(targetqqq)- TQQQ_fraction
        change_GUSH=(targetxop)- GUSH_fraction

        change_PALL=(targetpall)- PALL_fraction
        
        
        
        #-----orderpercent(symbol, targetweight,stage)
        @tenacity.retry(wait=tenacity.wait_fixed(1))
        def orderUPRO(stage):
            orderpercent(UPROsymbol, targetspy,stage)

        @tenacity.retry(wait=tenacity.wait_fixed(1))
        def orderTMF(stage):
            orderpercent(TMFsymbol, targettlt,stage)

        @tenacity.retry(wait=tenacity.wait_fixed(1))
        def orderUGLD(stage):
            orderpercent(UGLDsymbol, targetgld,stage)

        @tenacity.retry(wait=tenacity.wait_fixed(1))
        def orderTECL(stage):
            orderpercent(TECLsymbol, targetxlk,stage)

        @tenacity.retry(wait=tenacity.wait_fixed(1))
        def orderTQQQ(stage):
            orderpercent(TQQQsymbol, targetqqq,stage)

        @tenacity.retry(wait=tenacity.wait_fixed(1))
        def orderGUSH(stage):
            orderpercent(GUSHsymbol, targetxop,stage)

        @tenacity.retry(wait=tenacity.wait_fixed(1))
        def orderEDC(stage):
            orderpercent(EDCsymbol, targetvwo,stage)

        @tenacity.retry(wait=tenacity.wait_fixed(1))
        def orderDZK(stage):
            orderpercent(DZKsymbol, targetdzk,stage)

        @tenacity.retry(wait=tenacity.wait_fixed(1))
        def orderPALL(stage):
            orderpercent(PALLsymbol, targetpall,stage)

        
        #-----

        
        tokenDict = {change_UPRO:orderUPRO, change_TMF:orderTMF, change_UGLD:orderUGLD, change_TECL:orderTECL,
                    change_TQQQ:orderTQQQ,change_EDC:orderEDC, change_GUSH:orderGUSH, change_DZK:orderDZK,
                    change_PALL:orderPALL}

        lines = [change_TMF, change_UGLD,change_TECL, change_TQQQ,change_EDC, change_GUSH,change_DZK,
                change_UPRO,change_PALL   ]

        lines.sort()

        for line in lines:
            functionToCall = tokenDict[line]
            functionToCall(0)
            
        
        '''
        for line in lines:
            functionToCall = tokenDict[line]
            functionToCall(order stages in twap. if two stages total, put 1, if three put 2. 
            lists start from 0)
        for line in lines:
            functionToCall = tokenDict[line]
            functionToCall(1)
        
        for line in lines:
            functionToCall = tokenDict[line]
            functionToCall(2)  
        
        for line in lines:
            functionToCall = tokenDict[line]
            functionToCall(3) 
            '''
        
        
        print ("finished ordering")

        time.sleep(6)

        UPRO_fraction, UPRO_p=fraction_and_percent(UPROsymbol)

        TMF_fraction, TMF_p=fraction_and_percent(TMFsymbol)

        UGLD_fraction, UGLD_p=fraction_and_percent(UGLDsymbol)


        TQQQ_fraction, TQQQ_p=fraction_and_percent(TQQQsymbol)

        GUSH_fraction, GUSH_p=fraction_and_percent(GUSHsymbol)

        EDC_fraction, EDC_p=fraction_and_percent(EDCsymbol)

        TECL_fraction, TECL_p=fraction_and_percent(TECLsymbol)

        DZK_fraction, DZK_p=fraction_and_percent(DZKsymbol)

        PALL_fraction, PALL_p=fraction_and_percent(PALLsymbol)

        

        UPRO_portfolio=get_position('UPRO')#spy

        TMF_portfolio=get_position('TMF')#tlt
        UGLD_portfolio=get_position('UGLD')#gld
        TQQQ_portfolio=get_position('TQQQ')#qqq

        GUSH_portfolio=get_position('GUSH')#xop
        DZK_portfolio=get_position('DZK')#EFA" developed equities

        EDC_portfolio=get_position('EDC')#VWO emerging markets
        TECL_portfolio=get_position('TECL')#xlk

        PALL_portfolio=get_position('PALL')
        

        totalbalance= get_portfolio_value()
        #----------------------------------------------
        print ("current positions:")
        print ("UPRO({}%),TMF({}%),UGLD({}%),TQQQ({}%),GUSH({}%),DZK({}%), DZK({}%),EDC({}%),TECL({}%),PALL({}%),{} totalvalueusd".format(UPRO_p ,TMF_p,UGLD_p,TQQQ_p,GUSH_p,DZK_p,DZK_p,EDC_p,TECL_p,PALL_p,get_portfolio_value()))


        '''                          
        import smtplib
        from email.mime.text import MIMEText
        try:
            # Define to/from
            sender = 'XXXXXXXXXXXXXXXXXXXXXXXX'

            # Create message
            msg = MIMEText("UPRO({}%),TMF({}%),UGLD({}%),TQQQ({}%),GUSH({}%),DZK({}%), DZK({}%),EDC({}%),TECL({}%),PALL({}%),{} totalvalueusd".format(UPRO_p ,TMF_p,UGLD_p,TQQQ_p,GUSH_p,DZK_p,DZK_p,EDC_p,TECL_p,PALL_p,get_portfolio_value()))
            msg['Subject'] = "Portfolio Update"
            msg['From'] = sender
            #msg['To'] = recipient

            # Create server object with SSL option
            server = smtplib.SMTP_SSL('smtp.zoho.com',465)

            # Perform operations via server


            server = smtplib.SMTP_SSL('smtp.zoho.com',465)
            server.login('XXXXXXXXXXXX@zoho.com','XXXXXXXXXXXXXXX')
            server.sendmail(sender, [" XXXXXXXXXXXXX"], msg.as_string())
            server.quit()
        
            

        except:
            pass

        '''


        float_list = [arrow.now('US/Eastern').date(),
                      "{} UPRO({}%)".format(UPRO_portfolio, UPRO_p),
                      "{} UGLD({}%)".format(UGLD_portfolio, UGLD_p),
                      "{} TMF({}%)".format(TMF_portfolio, TMF_p),
                      "{} TECL({}%)".format(TECL_portfolio, TECL_p),
                      "{} TQQQ({}%)".format(TQQQ_portfolio, TQQQ_p),
                      "{} EDC({}%)".format(EDC_portfolio, EDC_p),
                      "{} DZK({}%)".format(DZK_portfolio, DZK_p),
                      "{} PALL({}%)".format(PALL_portfolio, PALL_p),
                      "{} GUSH({}%)".format(GUSH_portfolio, GUSH_p),
                      "{} totalvalueusd".format(totalbalance)]


        with open(r'portfolio_value.csv', 'a') as f:
            writer = csv.writer(f, delimiter=',')
            writer.writerow(float_list)


        float_list2 = [arrow.now('US/Eastern').date(),
                                                     UPRO_portfolio,
                                                     UPRO_p,UGLD_portfolio, UGLD_p,TMF_portfolio,
                                                     TMF_p,TECL_portfolio, TECL_p,TQQQ_portfolio, TQQQ_p,
                                                     EDC_portfolio, EDC_p,DZK_portfolio, DZK_p,
                                                     PALL_portfolio, PALL_p,GUSH_portfolio, GUSH_p, totalbalance]

        with open(r'portfolio_valuenumbers.csv', 'a') as f:
            writer = csv.writer(f, delimiter=',')
            writer.writerow(float_list2)





        if  date.today().isoweekday()==1:
            make_model(spy_df, onespy)
            make_model(spy_df, twospy)
            make_model(spy_df, threespy)
            make_model(spy_df, Fourspy)
            make_model(spy_df, Fivespy)


            make_model(qqq_df, oneqqq)
            make_model(qqq_df, twoqqq)
            make_model(qqq_df,  threeqqq)
            make_model(qqq_df, Fourqqq)
            make_model(qqq_df, Fiveqqq)



        elif  date.today().isoweekday()==2:
            make_model(tlt_df, onetlt)
            make_model(tlt_df,twotlt)
            make_model(tlt_df,threetlt)
            make_model(tlt_df, Fourtlt)
            make_model(tlt_df, Fivetlt)

            make_model(xop_df, onexop)
            make_model(xop_df, twoxop)
            make_model(xop_df,  threexop)
            make_model(xop_df, Fourxop)
            make_model(xop_df,Fivexop)



        elif  date.today().isoweekday()==3:
            make_model(xlk_df, onexlk)
            make_model(xlk_df,twoxlk)
            make_model(xlk_df,  threexlk)
            make_model(xlk_df, Fourxlk)
            make_model(xlk_df, Fivexlk)
            
            make_model(pall_df, onepall)
            make_model(pall_df, twopall)
            make_model(pall_df, threepall)
            make_model(pall_df, Fourpall)
            make_model(pall_df, Fivepall)



        elif  date.today().isoweekday()==4:
            make_model(efa_df, oneefa)
            make_model(efa_df,  twoefa)
            make_model(efa_df,  threeefa)
            make_model(efa_df, Fourefa)
            make_model(efa_df,  Fiveefa)

        elif date.today().isoweekday()==5:
            make_model(gld_df, onegld)
            make_model(gld_df, twogld)
            make_model(gld_df, threegld)
            make_model(gld_df, Fourgld)
            make_model(gld_df, Fivegld)

            make_model(vwo_df, onevwo)
            make_model(vwo_df, twovwo)
            make_model(vwo_df, threevwo)
            make_model(vwo_df, Fourvwo)
            make_model(vwo_df, Fivevwo)




    else:
        pass



    continue

